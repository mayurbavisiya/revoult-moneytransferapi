# Revoult-MoneyTransferAPI

There is no any framwork is used as stated in the problem statement so. It has used jackson library for rest service and jetty-server to make it stand alone.

How to run

Run Application.java from com.revoult package.(Default port is 8080)

URL Will be like,

http://localhost:8080/user/all 





http://localhost:8080/account/all


All the create operation used POST , Update operation are using PUT , Delete has used Delete and Select operation have used GET method.

Please check the screenshots for some of importants apis.

1) Create User
http://localhost:8080/user/create?userName=Mayur112&emailAddress=mayur.bavisya@gmail.com

2) Create Account
http://localhost:8080/account/create?userName=Mayur112&balance=100&currencyCode=EUR


3) Transaction (Transfer money from one account to other)
http://localhost:8080/transaction?currencyCode=USD&amount=100&fromAccountId=1&toAccountId=2
(Here currencyCode should be same from parameter with to and from User Account)

Transaction api is synchronized so it work expectedly during parallel hits.






