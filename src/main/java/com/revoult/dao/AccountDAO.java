package com.revoult.dao;

import java.math.BigDecimal;
import java.util.List;

import com.revoult.exception.CustomException;
import com.revoult.model.Account;
import com.revoult.model.UserTransaction;

public interface AccountDAO {

	List<Account> getAllAccounts() throws CustomException;

	Account getAccountById(long accountId) throws CustomException;

	long createAccount(Account account) throws CustomException;

	int deleteAccountById(long accountId) throws CustomException;

	int updateAccountBalance(long accountId, BigDecimal deltaAmount)
			throws CustomException;

	int transferAccountBalance(UserTransaction userTransaction)
			throws CustomException;
}
