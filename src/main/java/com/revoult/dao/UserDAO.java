package com.revoult.dao;

import java.util.List;

import com.revoult.exception.CustomException;
import com.revoult.model.User;

public interface UserDAO {

	List<User> getAllUsers() throws CustomException;

	User getUserById(long userId) throws CustomException;

	User getUserByName(String userName) throws CustomException;

	long insertUser(User user) throws CustomException;

	int updateUser(Long userId, User user) throws CustomException;

	int deleteUser(long userId) throws CustomException;

}
