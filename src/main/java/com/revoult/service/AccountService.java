package com.revoult.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.revoult.dao.DAOFactory;
import com.revoult.exception.CustomException;
import com.revoult.model.Account;
import com.revoult.model.MoneyUtil;

@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountService {

	private final DAOFactory daoFactory = DAOFactory
			.getDAOFactory(DAOFactory.H2);

	private static Logger log = Logger.getLogger(AccountService.class);

	@GET
	@Path("/all")
	public List<Account> getAllAccounts() throws CustomException {
		return daoFactory.getAccountDAO().getAllAccounts();
	}

	@GET
	@Path("/{accountId}")
	public Account getAccount(@PathParam("accountId") long accountId)
			throws CustomException {
		return daoFactory.getAccountDAO().getAccountById(accountId);
	}

	@GET
	@Path("/{accountId}/balance")
	public BigDecimal getBalance(@PathParam("accountId") long accountId)
			throws CustomException {
		final Account account = daoFactory.getAccountDAO().getAccountById(
				accountId);

		if (account == null) {
			throw new WebApplicationException("Account not found",
					Response.Status.NOT_FOUND);
		}
		return account.getBalance();
	}

	@POST
	@Path("/create")
	public Account createAccount(@QueryParam("userName") String userName,
			@QueryParam("balance") BigDecimal balance,
			@QueryParam("currencyCode") String currencyCode)
			throws CustomException {
		Account account = new Account(userName, balance, currencyCode);
		final long accountId = daoFactory.getAccountDAO()
				.createAccount(account);
		return daoFactory.getAccountDAO().getAccountById(accountId);
	}

	@PUT
	@Path("/{accountId}/deposit/{amount}")
	public Account deposit(@PathParam("accountId") long accountId,
			@PathParam("amount") BigDecimal amount) throws CustomException {

		if (amount.compareTo(MoneyUtil.zeroAmount) <= 0) {
			throw new WebApplicationException("Invalid Deposit amount",
					Response.Status.BAD_REQUEST);
		}

		daoFactory.getAccountDAO().updateAccountBalance(accountId,
				amount.setScale(4, RoundingMode.HALF_EVEN));
		return daoFactory.getAccountDAO().getAccountById(accountId);
	}

	@PUT
	@Path("/{accountId}/withdraw/{amount}")
	public Account withdraw(@PathParam("accountId") long accountId,
			@PathParam("amount") BigDecimal amount) throws CustomException {

		if (amount.compareTo(MoneyUtil.zeroAmount) <= 0) {
			throw new WebApplicationException("Invalid Deposit amount",
					Response.Status.BAD_REQUEST);
		}
		BigDecimal delta = amount.negate();
		if (log.isDebugEnabled())
			log.debug("Withdraw service: delta change to account  " + delta
					+ " Account ID = " + accountId);
		daoFactory.getAccountDAO().updateAccountBalance(accountId,
				delta.setScale(4, RoundingMode.HALF_EVEN));
		return daoFactory.getAccountDAO().getAccountById(accountId);
	}

	@DELETE
	@Path("/{accountId}")
	public Response deleteAccount(@PathParam("accountId") long accountId)
			throws CustomException {
		int deleteCount = daoFactory.getAccountDAO().deleteAccountById(
				accountId);
		if (deleteCount == 1) {
			return Response.status(Response.Status.OK).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

}
