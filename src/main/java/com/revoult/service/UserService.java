package com.revoult.service;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.revoult.dao.DAOFactory;
import com.revoult.exception.CustomException;
import com.revoult.model.User;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserService {

	private final DAOFactory daoFactory = DAOFactory
			.getDAOFactory(DAOFactory.H2);

	private static Logger log = Logger.getLogger(UserService.class);

	@GET
	@Path("/{userName}")
	public User getUserByName(@PathParam("userName") String userName)
			throws CustomException {
		if (log.isDebugEnabled())
			log.debug("Request Received for get User by Name " + userName);
		final User user = daoFactory.getUserDAO().getUserByName(userName);
		if (user == null) {
			throw new CustomException("User Not Found");
		}
		return user;
	}

	@GET
	@Path("/all")
	public List<User> getAllUsers() throws CustomException {
		return daoFactory.getUserDAO().getAllUsers();
	}

	@POST
	@Path("/create")
	public User createUser(@QueryParam("userName") String userName,
			@QueryParam("emailAddress") String emailAddress)
			throws CustomException {
		if (daoFactory.getUserDAO().getUserByName(userName) != null) {
			throw new CustomException("User name already exist");
		}
		final long uId = daoFactory.getUserDAO().insertUser(
				new User(userName, emailAddress));
		return daoFactory.getUserDAO().getUserById(uId);
	}

	@PUT
	@Path("/updateUser")
	public Response updateUser(@QueryParam("userId") long userId,
			@QueryParam("userName") String userName,
			@QueryParam("emailAddress") String emailAddress)
			throws CustomException {
		final int updateCount = daoFactory.getUserDAO().updateUser(userId,
				new User(userName, emailAddress));
		if (updateCount == 1) {
			return Response.status(Response.Status.OK).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@DELETE
	@Path("/{userId}")
	public Response deleteUser(@PathParam("userId") long userId)
			throws CustomException {
		int deleteCount = daoFactory.getUserDAO().deleteUser(userId);
		if (deleteCount == 1) {
			return Response.status(Response.Status.OK).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

}
