package com.revoult.service;

import java.math.BigDecimal;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.revoult.dao.DAOFactory;
import com.revoult.exception.CustomException;
import com.revoult.model.MoneyUtil;
import com.revoult.model.UserTransaction;

@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionService {

	private final DAOFactory daoFactory = DAOFactory
			.getDAOFactory(DAOFactory.H2);

	@POST
	public synchronized Response transferFund(
			@QueryParam("currencyCode") String currencyCode,
			@QueryParam("amount") BigDecimal amount,
			@QueryParam("fromAccountId") Long fromAccountId,
			@QueryParam("toAccountId") Long toAccountId) throws CustomException {

		String currency = currencyCode;
		if (MoneyUtil.INSTANCE.validateCcyCode(currency)) {
			int updateCount = daoFactory.getAccountDAO()
					.transferAccountBalance(
							new UserTransaction(currencyCode, amount,
									fromAccountId, toAccountId));
			if (updateCount == 2) {
				return Response.status(Response.Status.OK).build();
			} else {
				throw new WebApplicationException("Transaction failed",
						Response.Status.BAD_REQUEST);
			}

		} else {
			throw new WebApplicationException("Currency Code Invalid ",
					Response.Status.BAD_REQUEST);
		}

	}

}
